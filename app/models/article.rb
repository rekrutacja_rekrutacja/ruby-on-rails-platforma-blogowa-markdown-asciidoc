class Article < ActiveRecord::Base
  belongs_to :user
  has_many :comments

  validates :title, presence: true
  validates :content, presence: true

  def self.search(author)
  if author
    user = User.where(username: author).first
    if user
      self.where(user_id: user.id)
    else
      self.all
    end
  else
    self.all
  end
end
end
