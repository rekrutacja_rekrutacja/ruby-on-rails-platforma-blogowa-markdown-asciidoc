class AddTextTypeToComments < ActiveRecord::Migration
  def change
    add_column :comments, :text_type, :string
  end
end
