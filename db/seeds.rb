# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#CLEANING
Comment.destroy_all
Article.destroy_all
User.destroy_all


#ADMIN
admins = ["admin"]

admins.each do |name|
  User.create(
    username: name,
    password: "password",
    email: Faker::Internet.email,
    admin: 1
  )
end


#AUTHORS
authors = [
  "author1",
  "author2",
  "author3"
]

authors.each do |name|
  User.create(
    username: name,
    password: "password",
    email: Faker::Internet.email,
    author: 1
  )
end


#USERS
users = [
  "user1",
  "user2",
  "user3"
]

users.each do |name|
  User.create(
    username: name,
    password: "password",
    email: Faker::Internet.email
  )
end

10.times do
  User.create(
    username: Faker::Name.first_name,
    password: "password",
    email: Faker::Internet.email
  )
end


#ARTICLES
articles = [
  [
    "With greater visibility comes increased response",
    "Not wanting to disrupt business operations, I tested the device on a SPAN port that monitors traffic in and out of our network and in between. When I say “in between” I’m referring to a SPAN port that monitors traffic between the corporate network and our data center. That placement gave us visibility into attacks originating internally against our internal data center resources. (If we decide to purchase the PAN firewall or something similar, we’ll move it in-line and replace our current firewall.)

Lacking a 24/7 security operations center (someday maybe?), I set up the firewall to forward email alerts for events that I think are indicative of compromise. One thing I was very interested in was detecting threats against our source code repository, which I consider one of the five most critical assets in our organization. Sure enough, earlier this week I received an alert that an SSH brute-force attack against the server containing our source code had been detected. This alert triggers when more than 20 login attempts are made within 60 seconds."
  ],
  [
    "Finally, it's Apple Watch time",
    "It also means that on Monday morning, April 27, a lot of early adopters -- having spent the weekend figuring out how their new wearable works -- will stroll into the office without waiting for approval from IT.

The good news: Given Apple's history and the fact that it kick-started the BYOD movement with the iPhone and iPad, embracing the Apple Watch shouldn't be a big deal. Sure, early reviewers complained this week that it can be confusing and sluggish -- even as they acknowledged that it represents a paradigm shift in how people relate to technology."
  ],
  [
    "Growing into an IT project management job",
    "Steven Lefkowitz owes his job as an IT project manager at Partners HealthCare to his 75-pound golden retriever. In 2013 he struck up a conversation with the woman who was going to care for the dog while Lefkowitz and his wife were on vacation. \"She knew I was in job search mode,\" he recalls, and she told him to send her his resume. (Although she didn't work at Partners, she knew someone who worked there.)"
  ],
  [
    "Major Windows 10 'Redstone' updates tipped for 2016",
    "The final version of Windows 10 isn’t even available yet, but talk of what comes next is already popping up. Microsoft reportedly has two major updates to Windows 10 slated for 2016 that will parallel the release schedule for Windows 10 this year.

Codenamed \"Redstone\"—a nod to Minecraft, which Microsoft now owns—the two updates will show up around June and October of 2016, accordingon reports by Neowin’s Brad Sams and ZDNet’s Mary Jo Foley. Sams and Foley are both well-known Microsoft watchers with a strong track record for leaks based on information from sources inside the company."
  ],
  [
    "UK government's spying practices challenged at European human rights court",
    "The U.K. government’s mass surveillance practices will be challenged at the European Court of Human Rights.

Human rights and civil liberties organizations Amnesty International, Liberty and Privacy International have filed a joint application with the court, they announced on Friday."
  ],
  [
    "Transforming robot probes Fukushima reactor vessel",
    "Tokyo Electric Power on Friday sent a robot where no machine has gone before—inside the highly radioactive heart of a reactor at the crippled Fukushima Dai-Ichi nuclear power plant.

The robot, developed by Hitachi-GE Nuclear Energy and the International Research Institute for Nuclear Decommissioning (IRID), was inserted into the primary containment vessel (PCV) of reactor No. 1 at the plant, which was heavily damaged by the 2011 earthquake and subsequent tsunami that devastated northern Japan.

Tokyo Electric is taking the unprecedented step to better determine the state of melted-down fuel in the reactor as part of plans to dismantle the plant, a spokesman said. The No. 2 and No. 3 reactors also suffered meltdowns."
  ],
  [
    "ICANN seeks opinion on legality of '.sucks' registration process",
    "The body that manages the Internet domain name system has asked regulators in the U.S. and Canada to comment on the legality of the high prices and procedures used by Vox Populi Registry for registrations of ‘.sucks’ domain names by trademark owners.

The move Thursday by the Internet Corporation for Assigned Names and Numbers (ICANN) follows a recent letter from its Intellectual Property Constituency, which asked that the rollout of the new .sucks gTLD (generic top-level domain) should be halted.

The IPC, which represents the holders of trademarks and related intellectual property, described the registration scheme for the domain as predatory and designed to exploit trademark owners. It said Vox Populi had announced it would charge trademark owners US$2,499 and above to register domain names during the early ‘sunrise’ period."
  ],
  [
    "Altera spurns Intel, and might have done Intel a favor.",
    "After a few weeks of speculation, Altera has apparently walked away from a very generous acquisition offer from Intel. Altera makes field-programmable gate array (FPGA) processors, a field Intel does not play in, and it would have been a whole new market for Intel, which really needs a growth story these days.

Bloomberg reported that Altera walked away from an offer of $54 per share this week, a major premium over Altera's stock price, which was stalled in the mid-$30 range for more than three years. It shot up to $43 per share on news of the Intel merger talk."
  ],
  [
    "How to prep, in 7 steps, for Google's mobile search change",
    "With Google about to make a change that will affect how websites are ranked in its mobile search results, now is the time for companies to make sure their websites don't sink to the bottom of the pile.

On April 21, Google is changing the algorithm for its mobile search, placing websites that are deemed \"mobile friendly\" higher in mobile search rankings.

It also means that websites that have had high rankings in search results could lose their prominent positions if they are not optimized for mobile devices."
  ],
  [
    "Digital rights groups protest U.S. move to block digital transmissions",
    " A decision by a U.S. government agency prohibiting the transmission of 3D dental records into the U.S. could open the door to further content restrictions on the Internet, digital rights groups have said.

The heart of the question is whether the U.S. International Trade Commission can block digital goods, in additional to physical ones, from being imported to the U.S. The Motion Picture Association of America has watched the agency's decision closely, with an eye on using the USITC to block websites.

The USITC's decision concerns a patent dispute between two companies that make clear dental braces, but it could have larger consequences and is the wrong way to deal with infringement complaints, the rights groups said Friday in a letter to the agency."
  ],
  [
    "The 'Great Cannon' of China enforces Internet censorship",
    " China is deploying a tool that can be used to launch huge distributed denial-of-service (DDoS) attacks to enforce censorship. Researchers have dubbed it \"the Great Cannon.\"

The first time the tool was seen in action was during the massive DDoS attacks that hit software development platform GitHub last month. The attack sent large amounts of traffic to the site, targeting Chinese anti-censorship projects hosted there. It was the largest attack the site has endured in its history.

That attack was first thought to have been orchestrated using China's \"Great Firewall,\" a sophisticated ring of networking equipment and filtering software used by the government to exert strict control over Internet access in the country. The firewall is used to block sites like Facebook and Twitter as well as several media outlets."
  ],
  [
    "Sprint offers home delivery and setup of smartphones, tablets",
    "Faced with a highly competitive market, U.S. wireless operator Sprint is now offering to deliver and set up phones, tablets and other connected devices for free at homes, offices and other locations chosen by the customer.

The offer is currently limited to eligible upgrade customers, but starting September, new customers in selected markets will be able to choose the new Direct 2 You option, when buying online or through call centers.

Launching in Kansas City metropolitan area on Monday, the program will be expanded across the country using about 5,000 branded cars and employing 5,000 staff by year end. A rollout in Miami and Chicago is scheduled for April 20."
  ],
  [
    "Pivotal sets the stage for open-source in-memory computing",
    "Following through on a promise to open-source its data analysis software, Pivotal has released the source code that powers its GemFire in-memory database.

Opening up the code could give enterprise customers more input into what new features are added into future versions. For Pivotal, the move provides an entry to those corporate clients that have adopted policies of using open-source software whenever possible, said Roman Shaposhnik, Pivotal’s director of open source.

The company also hopes the software, released under the name Project Geode, will find a wider user base, one looking for big data analysis technologies speedier than Hadoop or Spark, Shaposhnik said."
  ],
  [
    "The mobile-enabled enterprise: Are we there yet?",
    "Modern mobile technology may have been born with the first iPhone, a quintessential consumer device, but it wasn’t long before the business possibilities began to emerge. Fast forward to today, and it’s difficult to find a company that hasn’t embraced phones and tablets for its employees to some degree.

It’s not difficult to see why. After all, the potential is nothing if not compelling: an untethered workforce equipped with easy-to-use tools for workers to be productive no matter where they are and at any time of day.

That allure, indeed, is surely part of the reason IT organizations will dedicate at least 25 percent of their software budgets to mobile application development, deployment and management by 2017, according to IDC. By that same year, in fact, the vast majority of line-of-business apps will be built for mobile-first consumption, IDC predicts—and for competitive necessity at least as often as for efficiency or productivity."
  ],
  [
    "Real money trade hits World of Warcraft game",
    "The introduction of a way to use real money to buy virtual cash for World of Warcraft has prompted a big change in the value of the game's gold.

The exchange rate for dollars fell by almost a third on the first day that Blizzard let people swap real cash for game gold.

At launch, players could spend $20 (£13) to get 30,000 gold coins to spend on gear in the fantasy game world.

But 24 hours later the same amount of cash netted players about 20,000.

Before now the only way that World of Warcraft players could artificially boost the fortunes of their characters was by visiting a grey-market site and surreptitiously buying gold from unlicensed vendors."
  ],
  [
    "PC shipments hit a six-year low as XP upgrades slow down",
    "A slowdown in Windows XP upgrades and the wait for Windows 10 sent worldwide PC shipments tumbling to a six-year low in the first quarter this year, according to IDC.

Worldwide PC shipments totaled 68.5 million units during the first quarter, declining by 6.7 percent compared with the same quarter the previous year, IDC said Thursday.

That is the lowest volume of first-quarter PC shipments since 2009, IDC said.

The PC shipments showed signs of stabilizing in the second half of last year as businesses replaced old Windows XP PCs with new Windows 7 systems. With pent-up demand to replace old PCs, Windows 10 could have a positive impact on PC shipments, said Rajani Singh, senior research analyst at IDC."
  ],
  [
    "Amazon offers network file storage in the cloud",
    "Amazon Web Services continues to chip away at the enterprise storage market, with plans for a new service designed to be a nimbler alternative to network attached storage (NAS) appliances.

The Amazon Elastic File System (EFS) will provide a shared, low-latency file system for organizations or project teams that need to share large files and access them quickly, such as a video production company.

“The file system is the missing element in the cloud today,” Amazon Web Services head Andy Jassy said Wednesday at the AWS Summit in San Francisco. The service is not yet available for full commercial use, though a preview will be available shortly."
  ]

]

articles.each do |title, content|
  Article.create(
    title: title,
    content: content,
    user_id: User.first.id + 1 + rand(authors.count),
    created_at: Faker::Time.between(14.days.ago, Time.now)
  )
end


#COMMENTS
for id in 1..articles.count do
  (0..3+rand(3)).each do
    Comment.create(
      content: Faker::Lorem.sentence,
      user_id: User.first.id + 4 + rand(13),
      article_id: id - 1 + Article.first.id
    )
  end
end


#################################################


#################################################
###                                           ###
###           Markdown and AsciiDoc           ###
###                                           ###
#################################################


#ARTICLES
articles = [
  [
    "m",
    "Markdown presentation",
    "An h1 header
============

Paragraphs are separated by a blank line.

2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists
look like:

  * this one
  * that one
  * the other one

Note that --- not considering the asterisk --- the actual text
content starts at 4-columns in.

> Block quotes are
> written like so.
>
> They can span multiple paragraphs,
> if you like.

Use 3 dashes for an em-dash. Use 2 dashes for ranges (ex., \"it's all
in chapters 12--14\"). Three dots ... will be converted to an ellipsis.
Unicode is supported. ☺



An h2 header
------------

Here's a numbered list:

 1. first item
 2. second item
 3. third item

Note again how the actual text starts at 4 columns in (4 characters
from the left side). Here's a code sample:

    # Let me re-iterate ...
    for i in 1 .. 10 { do-something(i) }

As you probably guessed, indented 4 spaces. By the way, instead of
indenting the block, you can use delimited blocks, if you like:

~~~
define foobar() {
    print \"Welcome to flavor country!\";
}
~~~

(which makes copying & pasting easier). You can optionally mark the
delimited block for Pandoc to syntax highlight it:

~~~python
import time
# Quick, count to ten!
for i in range(10):
    # (but not *too* quick)
    time.sleep(0.5)
    print i
~~~



### An h3 header ###

Now a nested list:

 1. First, get these ingredients:

      * carrots
      * celery
      * lentils

 2. Boil some water.

 3. Dump everything in the pot and follow
    this algorithm:

        find wooden spoon
        uncover pot
        stir
        cover pot
        balance wooden spoon precariously on pot handle
        wait 10 minutes
        goto first step (or shut off burner when done)

    Do not bump wooden spoon or it will fall.

Notice again how text always lines up on 4-space indents (including
that last line which continues item 3 above).

Here's a link to [a website](http://foo.bar), to a [local
doc](local-doc.html), and to a [section heading in the current
doc](#an-h2-header). Here's a footnote [^1].

[^1]: Footnote text goes here.

Tables can look like this:

size  material      color
----  ------------  ------------
9     leather       brown
10    hemp canvas   natural
11    glass         transparent

Table: Shoes, their sizes, and what they're made of

(The above is the caption for the table.) Pandoc also supports
multi-line tables:

--------  -----------------------
keyword   text
--------  -----------------------
red       Sunsets, apples, and
          other red or reddish
          things.

green     Leaves, grass, frogs
          and other things it's
          not easy being.
--------  -----------------------

A horizontal rule follows.

***

Here's a definition list:

apples
  : Good for making applesauce.
oranges
  : Citrus!
tomatoes
  : There's no \"e\" in tomatoe.

Again, text is indented 4 spaces. (Put a blank line between each
term/definition pair to spread things out more.)

Here's a \"line block\":

| Line one
|   Line too
| Line tree

and images can be specified like so:

![example image](http://www.ancestry.com/wiki/images/archive/a/a9/20100708215937!Example.jpg \"An exemplary image\")

And note that you can backslash-escape any punctuation characters
which you wish to be displayed literally, ex.: \\`foo\\`, \\*bar\\*, etc."
  ],
  [
    "m",
    "The truth about Macs in the enterprise",
    "### *Security, manageability, and lower TCO are why Macs should comprise 10 to 25 percent of your work PCs*

When I said last week that [Windows 10 won't save the PC](http://www.infoworld.com/article/2914152/microsoft-windows/windows-10-wont-save-the-pc.html), some Windows-addled IT folks said I was secretly suggesting that enterprises replace their PCs with Macs. That wasn't my intent, but those comments made me think about where the Mac fits in the enterprise and what causes so many IT organizations to be so emotionally opposed to having non-Windows PCs in their companies.

![example image](http://images.techhive.com/images/article/2015/04/new-macbook-primary-100578162-primary.idge.jpg \"An exemplary image\")

The truth is not black and white, but the following are true, even if many IT shops remain willfully ignorant to the facts and hang on to Mac realities and stereotypes from the 1990s:


  - Macs are more secure out of the box than Windows PCs.
  - Macs can be managed at scale.
  - Macs provide an operational recovery option that an all-Windows environment doesn't.
  - Macs do what most people need, though there are critical corporate needs that only Windows apps serve.
  - Macs cost the same as business-class PCs, and their total cost of ownership (TCO) is usually lower.
  - An all-Mac environment is as unreasonable as an all-Windows one.
  - Windows PCs, running Windows 7 today and Windows 10 in a few years, will remain the standard computing device for the majority of users.

    "
  ],
  [
    "a",
    "AsciiDoc preview",
    "
The Article Title
=================
Author's Name <authors@email.address>
v1.0, 2003-12


This is the optional preamble (an untitled section body). Useful for
writing simple sectionless documents consisting only of a preamble.

The abstract, preface, appendix, bibliography, glossary and
index section titles are significant ('specialsections').


:numbered!:
[abstract]
Example Abstract
----------------
The optional abstract (one or more paragraphs) goes here.

This document is an AsciiDoc article skeleton containing briefly
annotated element placeholders plus a couple of example index entries
and footnotes.

:numbered:

The First Section
-----------------
Article sections start at level 1 and can be nested up to four levels
deep.
footnote:[An example footnote.]
indexterm:[Example index entry]

And now for something completely different: ((monkeys)), lions and
tigers (Bengal and Siberian) using the alternative syntax index
entries.
(((Big cats,Lions)))
(((Big cats,Tigers,Bengal Tiger)))
(((Big cats,Tigers,Siberian Tiger)))
Note that multi-entry terms generate separate index entries.

An example block image:

image::http://hyperphysics.phy-astr.gsu.edu/hbase/electric/elepic/bulpara.jpg[lol]


[[X1]]
Sub-section with Anchor
~~~~~~~~~~~~~~~~~~~~~~~
Sub-section at level 2.

A Nested Sub-section
^^^^^^^^^^^^^^^^^^^^
Sub-section at level 3.

Yet another nested Sub-section
++++++++++++++++++++++++++++++
Sub-section at level 4.

This is the maximum sub-section depth supported by the distributed
AsciiDoc configuration.
footnote:[A second example footnote.]


The Second Section
------------------
Article sections are at level 1 and can contain sub-sections nested up
to four deep.

An example link to anchor at start of the <<X1,first sub-section>>.
indexterm:[Second example index entry]

An example link to a bibliography entry <<taoup>>.


:numbered!:

[appendix]
Example Appendix
----------------
AsciiDoc article appendices are just just article sections with
'specialsection' titles.

Appendix Sub-section
~~~~~~~~~~~~~~~~~~~~
Appendix sub-section at level 2.


[bibliography]
Example Bibliography
--------------------
The bibliography list is a style of AsciiDoc bulleted list.

[bibliography]
- [[[taoup]]] Eric Steven Raymond. 'The Art of Unix
  Programming'. Addison-Wesley. ISBN 0-13-142901-9.
- [[[walsh-muellner]]] Norman Walsh & Leonard Muellner.
  'DocBook - The Definitive Guide'. O'Reilly & Associates. 1999.
  ISBN 1-56592-580-7.


[glossary]
Example Glossary
----------------
Glossaries are optional. Glossaries entries are an example of a style
of AsciiDoc labeled lists.

[glossary]
A glossary term::
  The corresponding (indented) definition.

A second glossary term::
  The corresponding (indented) definition.


ifdef::backend-docbook[]
[index]
Example Index
-------------
////////////////////////////////////////////////////////////////
The index is normally left completely empty, it's contents being
generated automatically by the DocBook toolchain.
////////////////////////////////////////////////////////////////
endif::backend-docbook[]"
  ]
]

articles.each do |type, title, content|
  Article.create(
    title: title,
    content: content,
    user_id: User.first.id + 1 + rand(authors.count),
    created_at: Faker::Time.forward(1),
    text_type: type
  )
end
